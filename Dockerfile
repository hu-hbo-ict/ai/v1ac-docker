FROM jupyter/datascience-notebook

ARG NB_USER="ac"
USER root

RUN pip install python-gitlab

USER "$NB_USER"

ARG ac_gitlab_access_token
ENV AC_GITLAB_ACCESS_TOKEN=$ac_gitlab_access_token
